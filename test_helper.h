#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef void (*TestFn__)(void);

void registerTest__(TestFn__ fn, const char *name);

#define CONCAT2__(a, b) a ## b
#define CONCAT__(a, b) CONCAT2__(a, b)
#define TESTNAME__ CONCAT__(test_, __LINE__)

#define TEST(name)                                                             \
  static void TESTNAME__ (void);                                               \
  __attribute__((constructor))                                                 \
  static void CONCAT__(register, TESTNAME__) (void) {                          \
    registerTest__(&TESTNAME__, name);                                         \
  }                                                                            \
  static void TESTNAME__ ()

#define FORMAT_STRING__(thing)                                                 \
  _Generic((thing),                                                            \
    unsigned char: "%u",                                                       \
    char: "%c",                                                                \
    bool: "%s",                                                                \
    default: "%s")

#define FORMAT__(thing)                                                        \
  _Generic((thing),                                                            \
    unsigned char: (thing),                                                    \
    char: (thing),                                                             \
    bool: (thing) ? "true" : "false",                                          \
    default: "(unknown)")

#define assert_equal(a_, b_)                                                   \
  do {                                                                         \
    __auto_type a = (a_);                                                      \
    __auto_type b = (b_);                                                      \
                                                                               \
    if (a != b) {                                                              \
      fprintf(stderr, "Assertion failed in %s:%d, expected ",                  \
              __FILE__, __LINE__);                                             \
      fprintf(stderr, FORMAT_STRING__(a), FORMAT__(a));                        \
      fprintf(stderr, " to equal ");                                           \
      fprintf(stderr, FORMAT_STRING__(b), FORMAT__(b));                        \
      abort();                                                                 \
    }                                                                          \
  } while (0)
