#include "test_helper.h"
#include <stdlib.h>

typedef struct Test {
  TestFn__ fn;
  const char *name;

  struct Test *next;
} Test;

static Test *tests;

void registerTest__(TestFn__ fn, const char *name) {
  Test *newTest = malloc(sizeof(Test));

  newTest->fn = fn;
  newTest->name = name;
  newTest->next = tests;

  tests = newTest;
}

int main(int argc, const char **argv) {
  Test *currentTest = tests;

  while (currentTest) {
    fprintf(stderr, "Running test %s\n", currentTest->name);

    currentTest->fn();
    Test *prevTest = currentTest;
    currentTest = currentTest->next;

    free(prevTest);
  }
}
