#include "test_helper.h"

TEST("Test that some equal integers are equal") {
  assert_equal(6, 6);
}

TEST("Test that true is equal to true") {
  assert_equal(true, true);
}
